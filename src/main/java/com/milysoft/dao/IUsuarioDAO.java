package com.milysoft.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.milysoft.model.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Long> {
	
	Usuario findOneByUsername(String username);
}