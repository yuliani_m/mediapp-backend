package com.milysoft.service;

import java.util.List;

import com.milysoft.dto.ConsultaListaExamenDTO;
import com.milysoft.model.Consulta;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);

	void modificar(Consulta consulta);

	void eliminar(int idConsulta);

	Consulta listarId(int idConsulta);

	List<Consulta> listar();
}
